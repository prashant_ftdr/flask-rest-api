import requests, json, time
import pandas as pd

url = 'http://localhost:9200/vendorselectioncustomer-new/_search?size=124'
start = time.time()
# send request
response = requests.post(url)
# convert to dictionary
resDict = json.loads(json.dumps(response.json()))
sourceArray = []
for d in resDict['hits']['hits']:
    sourceArray.append(d['_source'])
df = pd.DataFrame(sourceArray)
end = time.time()
print("Time taken : ", (end-start), "s")
#print(df)