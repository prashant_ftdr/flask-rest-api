from flask import Flask, request, Response, json
import json as JSON
import logging as log
import sys
from bson.objectid import ObjectId
from pymongo import MongoClient
from splitio import get_factory
from splitio.exceptions import TimeoutException

app = Flask(__name__)

user_id = ''

class MongoAPI:
    def __init__(self, data):
        log.basicConfig(level=log.DEBUG, format='%(asctime)s %(levelname)s:\n%(message)s\n')
        # When only Mongo DB is running on Docker.
        self.client = MongoClient("mongodb://localhost:27017/")
        # When both Mongo and This application is running on Docker and we are using Docker Compose
        #self.client = MongoClient("mongodb://mymongo_1:27017/")

        collection = "users"
        cursor = self.client["vendorSelection"]
        self.collection = cursor[collection]
        self.data = data

    def readById(self):
        log.info('Reading All Data')
        document = self.collection.find({"_id" : ObjectId(self.data)}, {'_id': False})
        output = []
        for d in document:
            output.append(
                {
                "firstName": d['firstName'],
                "region": d['region'],
                }
            )
        return output

    def write(self, data):
        log.info('Writing Data')
        response = self.collection.insert_one(data)
        output = {'Status': 'Successfully Inserted',
                  'Document_ID': str(response.inserted_id)}
        return output


factory = get_factory('nfp7q2jo9q1lst0v7uehl7d579g2bq0p59r3')
try:
    factory.block_until_ready(5)
    @app.route("/users", methods=["GET"])
    def get():
        user_id = request.args.get('id')
        obj1 = MongoAPI(user_id)
        response = obj1.readById()
        dict = JSON.loads(json.dumps(response[0]))
        split = factory.client()
        regionTreatment = split.get_treatment(dict['region'], 'RegionSplit')
        if regionTreatment == 'on':
                return "Running model for denver region " + getUserSplitMessage(dict['firstName'])
        else:
                return "Running model for other region " + getUserSplitMessage(dict['firstName'])



    def getUserSplitMessage(name ):
        split = factory.client()
        userTreatment = split.get_treatment(name, 'UserSplit')
        if userTreatment == 'on':
            return " and for split user"
        else:
            return " and for non split user"
      #  return "dummy"


    @app.route("/users", methods=["POST"])
    def post():
        data = request.json
        if data is None or data == {}:
            return Response(
                response=json.dumps({"Error": "Please provide connection information"}),
                status=400,
                mimetype='application/json'
            )
        obj1 = MongoAPI(data)
        response = obj1.write(data)
        return Response(
            response=json.dumps(response),
            status=200,
            mimetype='application/json'
        )
except TimeoutException:
    # The SDK failed to initialize in 5 seconds. Abort!
    sys.exit()







