import eland as ed
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q

# name of the index we want to query
index_name = 'vendorselectioncustomer-new3'

# instantiating client connect to localhost by default
es = Elasticsearch()
ed = ed.DataFrame('localhost:9200', 'vendorselectioncustomer-new3')

print(ed.count())
query = {
        "query_string": {
            "fields": ["contract_id"],
            "query": "234043222"
        }
    }
print("contract id Query ==> ", ed.query('contract_id == 234043222'))

