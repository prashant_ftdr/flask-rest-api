# select OS
FROM alpine

# Copy everything which is present in my docker directory to working (/app)
COPY app /app

# Defining working directory
WORKDIR /app

# installing requirements
RUN pip3 install -r requirements.txt

# Exposing an internal port
EXPOSE 5001

# This command will be replaced if user provides any command by himself
CMD ["python3", "app.py"]