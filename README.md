# Flask REST API

You'll need a database named `vendorSelection` and collection named `users` on your mongoDB instance

You can change it to whatever you like, changing the values in `app.py` file

Sample Requests are:

POST Request : http://0.0.0.0:5001/users

```
{
    "firstName" : "Prashant",
    "region" : "Denver"
}
```
<hr>
GET Request : http://0.0.0.0:5001/users?id={user-id}

